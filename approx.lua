start,step,count,div = ...

if start == "help" then
 io.stderr:write("Usage: approximate <start> <step> <count> <div>\n")
 return
end

io.stderr:write("[info][read]Please enter a statement that should return 0\n")
eq = io.read()

if not eq then eq = "x" end

eq = eq:gsub("=","-")

f,r = load("x = ... return "..eq)

if not f then
 io.stderr:write("[error][compile]"..tostring(r).."\n")
 return
end

start = tonumber(start)
count  = tonumber(count)
div = tonumber(div)

if not start then start = 0 end
if not step then step = 0.1 end
if not count then count = 16 end
if not div then div = 10 end

x=start
i=0
while i<count do
 io.stderr:write("[info][count]"..i.." x:"..tostring(x).." step:"..tostring(step).."\n")
 i=i+1
 lop = 1
 c = 0
 ld = math.huge
 t = 0
 while true do
  ok,r = pcall(f,x)
  if ok then 
   --print("--------")
   --print(x,r,lop,c,ld,t)
   if r == 0 then print("= "..x) return end
   op = lop
   d = r
   if d<0 then d = -d end
   if ld < d then op = -lop t = t+1 end
   if t > 10 then x = x+(op*step) break end
   --print(d,t,op)
   ld = d
   lop =op
   if c%100 == 0 then
    if c > 0 then
     io.stderr:write("[info][state]c:"..c.." x:"..x.." d:"..d.." op:"..op.."\n") 
    end
  else 
   io.stderr:write("[error][run]"..r.."\n")
  end
  if lop == 0 then io.stderr:write("[info][stop]lop=0\n") return end
  ox = x
  x = x+(lop*step)
  if x == ox then io.stderr:write("[info][stop]old_x=x (step too small)\n") break end
  c=c+1
 end
 step = step/div;
end

print("~ "..tostring(math.abs(x)))