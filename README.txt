How to use:

Syntax:
lua brute-force.lua [<start> [<step> [<count> [<div>]]]]

<start> is the x it starts searching with
<step>  defines how much x will be in/decremented on the first iteration
<count> defines the number of iterations
<div>   defines by how much step will bedivded at the end of each iteration

Defualt values:

start = 0
step  = 0.1
count = 16
div   = 10

NOTE: the default values are good for most equations
NOTE: If an equation has multiple solutions it will pick the nearest one to the start
      It goes in the positive direction first so if the equation has tow results ...
        ...that are the same distance from the starting point it will pick the larger one

After launching the program you will be asked to enter a statement that should return 0
  This means an equation in lua syntax.
  NOTE: all equal signs ("=") will be replached by a substraction sign ("-") <- not an emoji :D
  for example:
   x=math.pi
   ((x*x)-(3.5*x))-(2) --will get solved  to return 0
                       --will return 0.5 on default settings and 4.0 if you start at for example 10
                       --lua approx.lua 10

WARNING: in some circumstances the algorythm will cycle forever (the famous halting problem)

OUTPUT:
All information is intented to be relatively easy to parse
Information will be printed to stderr
Results will be printed to stdout
a result starting with "= " means an f(x) == 0
a result starting with "~ " mkans it is an approximation


Information on information: (got it?)
 [error][compile] means the load() function that parses the statement has found an error in your statement | the program will exit
 [error][run]     means that pcall didn't return success on calling your function will print out pcalls error message and continue
 [info][count]<i> means it has started a new iteration
 [info][state]    is reporting the internal state of the program every hundred cycles
 [info][stop]     gives the reason why the program is stopping
 
 
 ALGORYTHM:(in a nutrshell: gradiand descent)
 the alogrythm works by reducing the distance of the output of the equation to 0
 if the distance decreases or stays equal it keeps going in that direction (it adds or substracts the step value)
 if the distance is increases it changes directions
 after 10 direction changes it ends the current iteration divides the step value by the div value
 and start's over again
 